import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'outstated';
import store from './Store';
import App from './components/App/App';
import './index.css';

ReactDOM.render(
  <Provider stores={[store]}>
    <App />
  </Provider>,
  document.getElementById('root')
);