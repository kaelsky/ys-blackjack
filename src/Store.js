import { useState, useEffect } from 'react';
import { fetchApi, getScore } from './Utils';

const api = {
  host: 'https://deckofcardsapi.com/api',
  timeOut: 150
}

const Store = () => {

  const [deck, setDeck] = useState([]);
  const [draw, setDraw] = useState([]);
  const [turn, setTurn] = useState('player');
  const [playerDeck, setPlayerDeck] = useState([]);
  const [dealerDeck, setDealerDeck] = useState([]);
  const [playerScore, setPlayerScore] = useState(0);
  const [dealerScore, setDealerScore] = useState(0);
  const [winner, setWinner] = useState(null);
  const [autoPlay, setAutoPlay] = useState(null);
  const [isServing, setIsServing] = useState(false);

  useEffect(() => {
    deck.deck_id && newGame();
  }, [deck.deck_id]);

  useEffect(() => {
    if (draw.cards) {
      const cardsCodes = encodeURI(draw.cards.map(item => item.code));

      give(cardsCodes, turn).then(() => {
        turn === 'player' ?
          setPlayerDeck([...playerDeck, draw.cards]) :
          setDealerDeck([...dealerDeck, draw.cards])
      });
    }
  }, [draw]);

  useEffect(() => {
    setPlayerScore(getScore(playerDeck))
  }, [playerDeck]);

  useEffect(() => {
    setDealerScore(getScore(dealerDeck));
  }, [dealerDeck]);

  useEffect(() => {
    if (!isServing) getWinner();
  }, [playerScore, dealerScore]);

  useEffect(() => {
    let running;
    if (autoPlay && !running && dealerScore < 21) {
      running = true;
      dealCards('dealer', 1);
    } else {
      setAutoPlay(false);
      running = false;
    }
  }, [autoPlay, dealerScore]);

  const shuffle = () => fetchApi(`${api.host}/deck/new/shuffle/?deck_count=1`, setDeck);

  const drawCards = count => fetchApi(`${api.host}/deck/${deck.deck_id}/draw/?count=${count}`, setDraw);

  const give = (cards, to) => fetchApi(`${api.host}/deck/${deck.deck_id}/pile/${to}/add/?cards=${cards}`, setDeck);

  const dealCards = async (to, nbr) => {
    await setTurn(to);

    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(drawCards(nbr));
      }, api.timeOut)
    });
  }
  
  const getWinner = () => {
    if (playerScore >= 21 || dealerScore >= 21) {
      dealerScore > 21 || playerScore === 21 ? setWinner('Player') : setWinner('Dealer')
    }
  }

  const pass = () => setAutoPlay(true);

  const reset = () => {
    shuffle();
    setDraw([]);
    setTurn(['dealer']);
    setPlayerDeck([]);
    setDealerDeck([]);
    setWinner(null)
  };

  const newGame = async () => {
    setIsServing(true);
    await dealCards('dealer', 1);
    await dealCards('dealer', 1);
    await dealCards('player', 1);
    await dealCards('player', 1);
    setIsServing(false);
  }

  return {
    deck, draw, shuffle, dealCards, reset,
    playerDeck, dealerDeck, playerScore, dealerScore, winner, pass
  };
};

export default Store;