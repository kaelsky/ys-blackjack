import React from 'react';
import { useStore } from 'outstated';
import store from '../../Store';
import './ActionBar.css';

function ActionBar() {

  const { dealCards, reset, playerScore, dealerScore, pass, deck } = useStore(store);

  return (
    <div className="ActionBar">

      <button className="action" onClick={reset}>NOUVELLE PARTIE</button>
      {
        (playerScore < 21 && dealerScore < 21) && deck.deck_id ?

          <React.Fragment>
            <button className="action" onClick={() => dealCards('player', 1)}>TIRER</button>
            <button className="action" onClick={() => pass()}>LAISSER</button>
          </React.Fragment> :
          null
      }
    </div>


  )
}

export default ActionBar;