import React from 'react';
import { appStyle } from '../../Config';
import logo from '../../logo.svg';
import Board from '../Board/Board';
import ActionBar from '../ActionBar/ActionBar';
import './App.css';

const styles = {
  app: {
    textAlign: 'center'
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1
  },
  logo: {
    padding: appStyle.padding.large
  }
};

function App() {

  return (
    <div className="App" style={styles.app}>
      <header className="App-header" style={styles.header}>
        <img className="logo" src={logo} alt="logo" style={styles.logo} />
      </header>
      <Board />
      <ActionBar />
    </div>
  );
}

export default App;
