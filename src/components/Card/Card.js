import React from 'react';
import './Card.css';

function Card(props) {
  return (
    <div className={`${props.actor}-card-wrapper`}>
      <div className="card" key={props.i}>
        <img src={props.item[0].images.png} alt={props.item[0].code} />
        <div className="card-back"></div>
      </div>
    </div>
  )
}

export default Card;