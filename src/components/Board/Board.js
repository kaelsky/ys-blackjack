import React, { useState, useEffect } from 'react';
import { useStore } from 'outstated';
import store from '../../Store';
import './Board.css';
import Card from '../Card/Card';

function Board() {

  const { playerDeck, dealerDeck, playerScore, dealerScore, winner } = useStore(store);

  const [notification, setNotification] = useState(<div><span role="img" aria-label="jsx-a11y/accessible-emoji">🃏</span> <span className="underlined">Yousign</span> Blackjack</div>);
  useEffect(() => {
    setTimeout(() => setNotification(null), 2200);
  }, [notification]);

  useEffect(() => {
    winner && setNotification(`${winner === 'Player' ? 'Joueur' : winner} l'emporte !`);
  }, [winner]);
  
  let playerCards = playerDeck.map((item, i) =>
    <Card item={item} key={i} actor='player' />
  );

  const dealerCards = dealerDeck.map((item, i) =>
    <Card item={item} key={i} actor={'dealer'} />
  );
  
  return (
    <div className="Board">

      <div className="dealer-board">
        <div className="dealer-info">
          <h1>Dealer</h1> 
          <p>Totalise <span className="underlined">{dealerScore}</span> points</p>
        </div>

        <div className="card-deck">
          {dealerCards}
        </div>
      </div>

      <div className="player-board">
        <div className="card-deck">
          {playerCards}
        </div>

        <div className="player-info">
          <h1>Joueur</h1> 
          <p>Totalise <span className="underlined">{playerScore}</span> points</p>
        </div>
      </div>

      <div className="logger">
        {notification ? <div className="logger-text">{notification}</div> : null}
      </div>

    </div>
  );
}

export default Board;
