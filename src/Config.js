const appStyle = {
  padding: {
    small: '8px',
    medium: '16px',
    large: '20px'
  },
  font: {
    p: '12px',
    h1: '28px',
    h2: '24px'
  }
}

export { appStyle}; 