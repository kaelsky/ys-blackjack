const fetchApi = (url, callback) =>
  fetch(url)
    .then(resp => resp.json())
    .then(data => callback ? callback(data) : data);

const getScore = (data) => {
  const heads = ['KING', 'QUEEN', 'JACK'];
  let hasAce = false;

  const calcScore = () => [...data].reduce((sum, item) => {
    let cardValue = item[0].value;
    const isHead = heads.includes(cardValue);
    const isAce = cardValue === 'ACE';

    if (isHead) {
      cardValue = 10;
    } else if (isAce) {
      hasAce = true;
      cardValue = 1;
    }

    return sum + parseInt(cardValue);
  }, 0);

  const addBonus = (score) => hasAce && score < 12 ? score + 10 : score;
  return addBonus(calcScore());
};

export { fetchApi, getScore };