# ys-blackjack

## Introduction

> Jeu de Blackjack mono-joueur simplifié, responsive et développé avec React à l'aide de l'API DeckOfCards : https://deckofcardsapi.com

## Spécifités  

> - React Hooks
>- No-redux (utilisation d'un Store basé sur les hooks permettant la gestion des states)

## Installation 
>`npm` / `yarn install`
>
>`npm` / `yarn start`

